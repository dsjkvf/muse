muse
====

## About

`muse` is a small shell script to choose music from a collection (either in whole, or the latest additions) in order to play it, to stream with [`strv`](https://bitbucket.org/dsjkvf/strv) or simply to copy to clipboard the path to the selected playlist.

## Usage

The following command line arguments are supported:

  - Actions:
    - to browse (default): `--browse`
    - to play selected music: `--play`
    - to stream with [`strv`](https://bitbucket.org/dsjkvf/strv): `--stream`
  - Scopes:
    - to browse, play or stream the whole collection (default): `--all`
    - to browse, play or stream some (30 by default) latest additions: `--latest`
    - to play or stream the current directory: `--here`

### Examples

    muse --latest --play
    muse # which is the same as muse --all --browse
    muse --here --stream

### Aliases

Introducing shell aliases may be of some interest -- like this:

    # browse the whole music catalog
    alias muz='muse'
    # browse the latest additions to the music catalog
    alias mul='muse --latest'
    # play (choose from the whole catalog unless stated overwise, like 'mup --latest')
    alias mup='muse --play'
    # play the current folder
    alias muh='muse --here --play'
    # stream (choose from the whole catalog unless stated overwise, like 'mus --latest')
    alias mus='muse --stream'

## Prerequisites

[`fzf`](https://github.com/junegunn/fzf.vim), [fzy](https://github.com/jhawthorn/fzy) or any other fuzzy finder of your choice.

## Configuration

`muse` also has some configuration options, which can be tweaked directly in the script in the section `OPTIONS`:

  - the fuzzy finder:

    FUZZY='fzy'

  - the location of one's music collection:

    LOCATION='/Users/s/Downloads/#music'

  - the number of the latest additions to show (which can be approximate if there are any other garbage sub-folders, see the `GREPPAT` option):

    LATEST=30

  - the additional command allowing to exclude some possible garbage (by default, shows only folders, which have a square bracket in the name -- i.e., the ones named like this: `The Doors - [1967] Strange Days`):

    FILTER='grep "\["'
